> - List price: $249 USD
> - [**A free license for Premium members**](https://forum.ircam.fr/subscribe/?subscription=login) - [ask for the voucher](https://forum.ircam.fr/contact/)
> - Distributed by [Plugivery](https://www.ircamlab.com/products/p1680-TS2/)
> - Only available by downloading
> - [Technical Support](https://help.ircamlab.com)
>[Online course]( https://www.ircam.fr/transmission/formations-professionnelles/)


![TS2](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Lab%20TS/ircamlab_ts2_gui.png)

ircamLAB and their official distributor Plugivery proudly announce that they have released a new version of their flagship TS (Time-Stretch) application, now renamed "TS2".

The ircamLab TS2 application sets a new standard in digital audio editing. It is a powerful audio toolbox application for professionals to edit or magically transform, time-stretch and manipulate audio files.

The new TS2 is now a full mono/stereo editor. It can edit any audio regions (Copy, Cut, Paste), transpose, "Transient Design", and offer remix, extensive mastering capabilities and much more with unprecedented accuracy.

The TS2 can from now on apply external VST/AU effects from third party plug-in manufacturers.

The new TS2 application is a comprehensive suite of tools focused on the acclaimed Super VP Engine to process complex audio editing tasks that includes full automation of parameters such as; Pitch, Transposition, Time Stretching, Remix Noise, Sinus & Transient design.

Post-production professionals, audio engineers, sound designers, musicians and video editors alike use TS2 to transform recordings into production-ready audio files.

TS2 has been re-designed from scratch thanks to the amazing feedback and requests collected by users over the years. It processes an accurate real-time sonogram display and handles full automation over the "Master Module" with multi-format plug-in insertion.
 

## What's New in the TS2 Audio Editor ##

- Full Audio Editing Capabilities
- Copy, Cut, Paste, Loop functions on Audio Regions
- Full Automation over the Super VP™ engine
- Individual Volume Control with Fade In & Out
- Multi Regions Editing with Cross-fades
- Unlimited Undo's
- Multiple Audio Files Management
- Multiple Documents
- Multi Ruler Control (time, second)
- Loop and Tempo Calculator                         
- Multi Windows and Multi-Screen Visualization                
- Real-Time Sonogram View
- Export of the Sonogram Visual (PNG) & Command Lines    
- Multi-format VST2, VST3 and AU plug-in insertion

## About ##

- **[ircamLAB](https://www.ircamlab.com)**

"ircamLAB", an affiliate to [IRCAM](https://www.ircam.fr) (Institute for Research and Creation in Acoustics and Music), is dedicated to manufacturing powerful audio software and plug-ins, based on decades of research and technological innovation.

ircamLAB is the manufacturing brand owned by "[ircam Amplify](https://ircamamplify.com)". Created in 2019, ircam Amplify, is the marketing subsidiary of IRCAM. It markets the «Power of Sound. 

- **[Plugivery](https://www.plugivery.com)**

Plugivery is a self financed B2B (business to business) distribution company whose purpose is to introduce audio software products into hundreds of music stores all around the world carefully considering every need of a truly professional distribution service based on electronic delivery.

The name Plugivery comes from the combination of the words “Plug-in” and “Delivery”.

![IRCAM Lab](https://forum.ircam.fr/media/uploads/Softwares/The%20Snail/logo_ircam_lab.png)
![Plugivery](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Lab%20TS/logo_big.png)
